﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CustomToolStrip
{
    [System.Windows.Forms.Design.ToolStripItemDesignerAvailability(
        System.Windows.Forms.Design.ToolStripItemDesignerAvailability.MenuStrip |
        System.Windows.Forms.Design.ToolStripItemDesignerAvailability.ContextMenuStrip)]
    public class ToolStripInlineBrushPicker : ToolStripControlHost
    {
        #region events
        public event BrushSelectedEventHandler BrushSelected;
        protected virtual void OnBrushSelected(BrushSelectedEventArgs e)
        {
            if (BrushSelected != null) BrushSelected(this, e);
        }
        #endregion

        #region properties
        /// <summary>
        /// ColorPanel
        /// </summary>
        public BrushTilePanel BrushPanel
        {
            get { return (BrushTilePanel)Control; }
        }

        /// <summary>
        /// CellWidth
        /// </summary>
        public int CellWidth
        {
            get { return BrushPanel.CellWidth; }
            set { BrushPanel.CellWidth = value; }
        }

        /// <summary>
        /// CellHeight
        /// </summary>
        public int CellHeight
        {
            get { return BrushPanel.CellHeight; }
            set { BrushPanel.CellHeight = value; }
        }

        /// <summary>
        /// Columns
        /// </summary>
        public int Columns
        {
            get { return BrushPanel.Columns; }
            set { BrushPanel.Columns = value; }
        }

        /// <summary>
        /// Rows
        /// </summary>
        public int Rows
        {
            get { return BrushPanel.Rows; }
            set { BrushPanel.Rows = value; }
        }

        /// <summary>
        /// CellPadding
        /// </summary>
        public int CellPadding
        {
            get { return BrushPanel.CellPadding; }
            set { BrushPanel.CellPadding = value; }
        }

        /// <summary>
        /// ColorList
        /// </summary>
        public List<int> BrushList
        {
            get { return BrushPanel.BrushList; }
            set { BrushPanel.BrushList = value; }
        }

        /// <summary>
        /// CurrentColor
        /// </summary>
        public int CurrentMode
        {
            get { return BrushPanel.CurrentMode; }
            set { BrushPanel.CurrentMode = value; }
        }
        #endregion

        #region methods
        /// <summary>
        /// Constructor
        /// </summary>
        public ToolStripInlineBrushPicker()
            : base(new BrushTilePanel())
        { }

        public ToolStripInlineBrushPicker(Control c)
            : base(c)
        { }
        #endregion

        #region handlers
        protected override void OnSubscribeControlEvents(Control control)
        {
            base.OnSubscribeControlEvents(control);

            BrushTilePanel panel = control as BrushTilePanel;
            panel.BrushSelected += panel_BrushSelected;
        }

        protected override void OnUnsubscribeControlEvents(Control control)
        {
            base.OnUnsubscribeControlEvents(control);

            BrushTilePanel panel = control as BrushTilePanel;
            panel.BrushSelected -= panel_BrushSelected;
        }

        void panel_BrushSelected(object sender, BrushSelectedEventArgs e)
        {
            OnBrushSelected(e);
        }
        #endregion
    }
}
