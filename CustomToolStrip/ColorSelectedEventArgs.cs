﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace CustomToolStrip
{
    public delegate void ColorSelectedEventHandler(object sender, ColorSelectedEventArgs e);

    public class ColorSelectedEventArgs : EventArgs
    {
        #region properties
        public Color SelectedColor { get; set; }
        #endregion

        #region methods
        public ColorSelectedEventArgs(Color c)
        {
            SelectedColor = c;
        }
        #endregion
    }

}
