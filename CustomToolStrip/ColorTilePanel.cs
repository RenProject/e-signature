﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CustomToolStrip
{
    public partial class ColorTilePanel : Control
    {
        #region events
        public event ColorSelectedEventHandler ColorSelected;
        protected virtual void OnColorSelected(ColorSelectedEventArgs e)
        {
            if (ColorSelected != null) ColorSelected(this, e);
        }
        #endregion

        #region properties
        private int _cellWidth = 40;
        /// <summary>
        /// CellWidth
        /// </summary>
        public int CellWidth
        {
            get { return _cellWidth; }
            set { _cellWidth = value; UpdateProperties(); }
        }

        private int _cellHeight = 40;
        /// <summary>
        /// CellHeight
        /// </summary>
        public int CellHeight
        {
            get { return _cellHeight; }
            set { _cellHeight = value; UpdateProperties(); }
        }

        private int _columns = 5;
        /// <summary>
        /// Columns
        /// </summary>
        public int Columns
        {
            get { return _columns; }
            set { _columns = value; UpdateProperties(); }
        }

        private int _rows = 1;
        /// <summary>
        /// Rows
        /// </summary>
        public int Rows
        {
            get { return _rows; }
            set { _rows = value; UpdateProperties(); }
        }

        private int _cellPadding = 3;
        /// <summary>
        /// CellPadding
        /// </summary>
        public int CellPadding
        {
            get { return _cellPadding; }
            set { _cellPadding = value; UpdateProperties(); }
        }

        private List<Color> _colorList = new List<Color>() { Color.Black, Color.Red, Color.Blue, Color.Green, Color.Gold};
        /// <summary>
        /// ColorList
        /// </summary>
        public List<Color> ColorList
        {
            get
            {
                return _colorList;
            }
            set
            {
                _colorList = value;
                UpdateProperties();
            }
        }

        private Color _currentColor = Color.Transparent;
        /// <summary>
        /// CurrentColor
        /// </summary>
        public Color CurrentColor
        {
            get
            {
                return _currentColor;
            }
            set
            {
                _currentColor = value;
                _currentIdx = ColorList.IndexOf(_currentColor);
                UpdateProperties();
            }
        }
        #endregion

        #region private properties
        /// <summary>
        /// Color Cell Width Outside
        /// </summary>
        private int CellWidthOut
        {
            get { return CellWidth + CellPadding * 2 + 1; }
        }

        /// <summary>
        /// Color Cell Height Outside
        /// </summary>
        private int CellHeightOut
        {
            get { return CellHeight + CellPadding * 2 + 1; }
        }
        #endregion

        #region fields
        private int _currentIdx = -1;

        private int _selectColIdx = -1;
        private int _selectRowIdx = -1;

        private int _selectingColorIndex = -1;

        private Bitmap _colorTile = null;
        #endregion

        #region methods
        /// <summary>
        /// Constructor
        /// </summary>
        public ColorTilePanel()
        {
            InitializeComponent();

            UpdateProperties();
        }

        /// <summary>
        /// UpdateProperties
        /// </summary>
        private void UpdateProperties()
        {
            Width = Columns * CellWidthOut + Padding.Left + Padding.Right;
            Height = Rows * CellHeightOut + Padding.Left + Padding.Right;
            
            _colorTile = CreateColorTile();
            Invalidate();
        }

        /// <summary>
        /// CreateColorTile
        /// </summary>
        /// <returns></returns>
        private Bitmap CreateColorTile()
        {
            Bitmap bmp = new Bitmap(Width, Height);

            using (Graphics g = Graphics.FromImage(bmp))
            {
                for (int idx = 0; idx < ColorList.Count(); idx++)
                {
                    if (idx >= Columns * Rows) break;

                    int x = idx % Columns;
                    int y = idx / Columns;

                    var r = new Rectangle((Padding.Left + CellPadding) + x * CellWidthOut, (Padding.Top + CellPadding) + y * CellHeightOut, CellWidth, CellHeight);

                    //g.FillRectangle(new SolidBrush(ColorList[idx]), r);
                    //g.DrawRectangle(new Pen(Color.LightGray), r);

                    g.DrawEllipse(new Pen(Color.FromArgb(255, Color.Gray)), r);
                    g.FillEllipse(new SolidBrush(Color.FromArgb(120, ColorList[idx])), r);
                }
            }

            return bmp;
        }
        #endregion

        #region event handlers
        /// <summary>
        /// OnMouseClick
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseClick(MouseEventArgs e)
        {
            base.OnMouseClick(e);

            if (e.Button == MouseButtons.Left)
            {
                _selectColIdx = (e.Location.X - Padding.Left) / CellWidthOut;
                _selectRowIdx = (e.Location.Y - Padding.Top) / CellHeightOut;

                if (_selectColIdx >= 0 & _selectRowIdx >= 0 & _selectColIdx < Columns & _selectRowIdx < Rows)
                {
                    int idx = _selectColIdx + _selectRowIdx * Columns;
                    if (idx < ColorList.Count())
                    {
                        CurrentColor = ColorList[idx];
                        OnColorSelected(new ColorSelectedEventArgs(CurrentColor));
                    }
                }
            }
        }

        /// <summary>
        /// OnMouseMove
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            _selectColIdx = (e.Location.X - Padding.Left) / CellWidthOut;
            _selectRowIdx = (e.Location.Y - Padding.Top) / CellHeightOut;
            int wkCur = _selectColIdx + _selectRowIdx * Columns;

            if (_selectColIdx >= 0 & _selectRowIdx >= 0 & _selectColIdx < Columns & _selectRowIdx < Rows & wkCur != _selectingColorIndex)
            {
                Invalidate();
            }

            _selectingColorIndex = _selectColIdx + _selectRowIdx * Columns;
        }

        /// <summary>
        /// OnMouseLeave
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);

            _selectColIdx = -1;
            _selectRowIdx = -1;
            _selectingColorIndex = -1;
            Invalidate();
        }

        /// <summary>
        /// OnPaintBackground
        /// </summary>
        /// <param name="pevent"></param>
        protected override void OnPaintBackground(PaintEventArgs pevent)
        {
            //base.OnPaintBackground(pevent);
        }

        /// <summary>
        /// OnPaint
        /// </summary>
        /// <param name="pe"></param>
        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);

            Bitmap bmp = new Bitmap(Width, Height);
            using (Graphics gg = Graphics.FromImage(bmp))
            {
                gg.FillRectangle(new SolidBrush(this.BackColor), 0, 0, bmp.Width, bmp.Height);

                { // disp cell on mouse postion
                    if (_selectColIdx >= 0 & _selectRowIdx >= 0)
                    {
                        var r = new Rectangle(Padding.Left + _selectColIdx * CellWidthOut, Padding.Top + _selectRowIdx * CellHeightOut, CellWidthOut, CellHeightOut);
                        gg.FillRectangle(new SolidBrush(Color.SkyBlue), r);
                    }
                }

                { // disp color tile
                    if (_colorTile != null)
                    {
                        gg.DrawImage(_colorTile, 0, 0);
                    }
                }
            }

            Graphics g = pe.Graphics;
            g.DrawImage(bmp, 0, 0);

            { // disp current color
                if (_currentIdx >= 0 & _currentIdx < ColorList.Count())
                {
                    int currentX = _currentIdx % Columns;
                    int currentY = _currentIdx / Columns;

                    int l = Padding.Left + currentX * CellWidthOut;
                    int t = Padding.Top + currentY * CellHeightOut;
                    var rect = new Win32Api.RECT() { left = l, top = t, right = l + CellWidthOut, bottom = t + CellHeightOut };
                    Win32Api.DrawFocusRect(g.GetHdc(), ref rect);
                    g.ReleaseHdc();
                }
            }
        }
        #endregion
    }
}
