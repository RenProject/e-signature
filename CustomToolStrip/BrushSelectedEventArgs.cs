﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace CustomToolStrip
{
    public delegate void BrushSelectedEventHandler(object sender, BrushSelectedEventArgs e);

    public class BrushSelectedEventArgs : EventArgs
    {
        #region properties
        public int SelectedMode { get; set; }
        #endregion

        #region methods
        public BrushSelectedEventArgs(int mode)
        {
            SelectedMode = mode;
        }
        #endregion
    }

}
