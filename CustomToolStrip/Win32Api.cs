﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace CustomToolStrip
{
    public class Win32Api
    {
        [StructLayout(LayoutKind.Sequential)]
        public struct RECT
        { public int left; public int top; public int right; public int bottom; }

        [DllImport("user32")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool DrawFocusRect(IntPtr hDC, ref RECT lpRect);
    }
}
