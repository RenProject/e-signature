﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CustomToolStrip.Properties;

namespace CustomToolStrip
{
    public partial class BrushTilePanel : Control
    {
        #region events
        public event BrushSelectedEventHandler BrushSelected;
        protected virtual void OnBrushSelected(BrushSelectedEventArgs e)
        {
            if (BrushSelected != null) BrushSelected(this, e);
        }
        #endregion

        #region properties
        private int _cellWidth = 40;
        /// <summary>
        /// CellWidth
        /// </summary>
        public int CellWidth
        {
            get { return _cellWidth; }
            set { _cellWidth = value; UpdateProperties(); }
        }

        private int _cellHeight = 40;
        /// <summary>
        /// CellHeight
        /// </summary>
        public int CellHeight
        {
            get { return _cellHeight; }
            set { _cellHeight = value; UpdateProperties(); }
        }

        private int _columns = 2;
        /// <summary>
        /// Columns
        /// </summary>
        public int Columns
        {
            get { return _columns; }
            set { _columns = value; UpdateProperties(); }
        }

        private int _rows = 1;
        /// <summary>
        /// Rows
        /// </summary>
        public int Rows
        {
            get { return _rows; }
            set { _rows = value; UpdateProperties(); }
        }

        private int _cellPadding = 3;
        /// <summary>
        /// CellPadding
        /// </summary>
        public int CellPadding
        {
            get { return _cellPadding; }
            set { _cellPadding = value; UpdateProperties(); }
        }

        private List<int> _brushList = new List<int>() { 1, 2 };
        /// <summary>
        /// ColorList
        /// </summary>
        public List<int> BrushList
        {
            get
            {
                return _brushList;
            }
            set
            {
                _brushList = value;
                UpdateProperties();
            }
        }

        private int _currentMode = 1;
        /// <summary>
        /// CurrentColor
        /// </summary>
        public int CurrentMode
        {
            get
            {
                return _currentMode;
            }
            set
            {
                _currentMode = value;
                _currentIdx = BrushList.IndexOf(_currentMode);
                UpdateProperties();
            }
        }
        #endregion

        #region private properties
        /// <summary>
        /// Color Cell Width Outside
        /// </summary>
        private int CellWidthOut
        {
            get { return CellWidth + CellPadding * 2 + 1; }
        }

        /// <summary>
        /// Color Cell Height Outside
        /// </summary>
        private int CellHeightOut
        {
            get { return CellHeight + CellPadding * 2 + 1; }
        }
        #endregion

        #region fields
        private int _currentIdx = -1;

        private int _selectColIdx = -1;
        private int _selectRowIdx = -1;

        private int _selectingModeIndex = -1;

        private Bitmap _brushTile = null;
        #endregion

        #region methods
        /// <summary>
        /// Constructor
        /// </summary>
        public BrushTilePanel()
        {
            InitializeComponent();

            UpdateProperties();
        }

        /// <summary>
        /// UpdateProperties
        /// </summary>
        private void UpdateProperties()
        {
            Width = Columns * CellWidthOut + Padding.Left + Padding.Right;
            Height = Rows * CellHeightOut + Padding.Left + Padding.Right;
            
            _brushTile = CreateBrushTile();
            Invalidate();
        }

        /// <summary>
        /// CreateColorTile
        /// </summary>
        /// <returns></returns>
        private Bitmap CreateBrushTile()
        {
            Bitmap bmp = new Bitmap(Width, Height);

            using (Graphics g = Graphics.FromImage(bmp))
            {
                for (int idx = 0; idx < BrushList.Count(); idx++)
                {
                    if (idx >= Columns * Rows) break;

                    int x = idx % Columns;
                    int y = idx / Columns;

                    var r = new Rectangle((Padding.Left + CellPadding) + x * CellWidthOut, (Padding.Top + CellPadding) + y * CellHeightOut, CellWidth, CellHeight);

                    if(BrushList[idx] == 1)
                    {
                        Bitmap image = Resources.pen;
                        g.DrawImage(image, r.X, r.Y, CellWidth, CellHeight);
                    }
                    else if (BrushList[idx] == 2)
                    {
                        Bitmap image = Resources.highlighter;
                        g.DrawImage(image, r.X, r.Y, CellWidth, CellHeight);
                    }

                }
            }

            return bmp;
        }

        #endregion

        #region event handlers
        /// <summary>
        /// OnMouseClick
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseClick(MouseEventArgs e)
        {
            base.OnMouseClick(e);

            if (e.Button == MouseButtons.Left)
            {
                _selectColIdx = (e.Location.X - Padding.Left) / CellWidthOut;
                _selectRowIdx = (e.Location.Y - Padding.Top) / CellHeightOut;

                if (_selectColIdx >= 0 & _selectRowIdx >= 0 & _selectColIdx < Columns & _selectRowIdx < Rows)
                {
                    int idx = _selectColIdx + _selectRowIdx * Columns;
                    if (idx < BrushList.Count())
                    {
                        CurrentMode = BrushList[idx];
                        OnBrushSelected(new BrushSelectedEventArgs(CurrentMode));
                    }
                }
            }
        }

        /// <summary>
        /// OnMouseMove
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            _selectColIdx = (e.Location.X - Padding.Left) / CellWidthOut;
            _selectRowIdx = (e.Location.Y - Padding.Top) / CellHeightOut;
            int wkCur = _selectColIdx + _selectRowIdx * Columns;

            if (_selectColIdx >= 0 & _selectRowIdx >= 0 & _selectColIdx < Columns & _selectRowIdx < Rows & wkCur != _selectingModeIndex)
            {
                Invalidate();
            }

            _selectingModeIndex = _selectColIdx + _selectRowIdx * Columns;
        }

        /// <summary>
        /// OnMouseLeave
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);

            _selectColIdx = -1;
            _selectRowIdx = -1;
            _selectingModeIndex = -1;
            Invalidate();
        }

        /// <summary>
        /// OnPaintBackground
        /// </summary>
        /// <param name="pevent"></param>
        protected override void OnPaintBackground(PaintEventArgs pevent)
        {
            //base.OnPaintBackground(pevent);
        }

        /// <summary>
        /// OnPaint
        /// </summary>
        /// <param name="pe"></param>
        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);

            Bitmap bmp = new Bitmap(Width, Height);
            using (Graphics gg = Graphics.FromImage(bmp))
            {
                gg.FillRectangle(new SolidBrush(this.BackColor), 0, 0, bmp.Width, bmp.Height);

                { // disp cell on mouse postion
                    if (_selectColIdx >= 0 & _selectRowIdx >= 0)
                    {
                        var r = new Rectangle(Padding.Left + _selectColIdx * CellWidthOut, Padding.Top + _selectRowIdx * CellHeightOut, CellWidthOut, CellHeightOut);
                        gg.FillRectangle(new SolidBrush(Color.SkyBlue), r);
                    }
                }

                { // disp color tile
                    if (_brushTile != null)
                    {
                        gg.DrawImage(_brushTile, 0, 0);
                    }
                }
            }

            Graphics g = pe.Graphics;
            g.DrawImage(bmp, 0, 0);

            { // disp current color
                if (_currentIdx >= 0 & _currentIdx < BrushList.Count())
                {
                    int currentX = _currentIdx % Columns;
                    int currentY = _currentIdx / Columns;

                    int l = Padding.Left + currentX * CellWidthOut;
                    int t = Padding.Top + currentY * CellHeightOut;
                    var rect = new Win32Api.RECT() { left = l, top = t, right = l + CellWidthOut, bottom = t + CellHeightOut };
                    Win32Api.DrawFocusRect(g.GetHdc(), ref rect);
                    g.ReleaseHdc();
                }
            }
        }
        #endregion
    }
}
