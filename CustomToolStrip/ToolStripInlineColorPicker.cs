﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CustomToolStrip
{
    [System.Windows.Forms.Design.ToolStripItemDesignerAvailability(
        System.Windows.Forms.Design.ToolStripItemDesignerAvailability.MenuStrip |
        System.Windows.Forms.Design.ToolStripItemDesignerAvailability.ContextMenuStrip)]
    public class ToolStripInlineColorPicker : ToolStripControlHost
    {
        #region events
        public event ColorSelectedEventHandler ColorSelected;
        protected virtual void OnColorSelected(ColorSelectedEventArgs e)
        {
            if (ColorSelected != null) ColorSelected(this, e);
        }
        #endregion

        #region properties
        /// <summary>
        /// ColorPanel
        /// </summary>
        public ColorTilePanel ColorPanel
        {
            get { return (ColorTilePanel)Control; }
        }

        /// <summary>
        /// CellWidth
        /// </summary>
        public int CellWidth
        {
            get { return ColorPanel.CellWidth; }
            set { ColorPanel.CellWidth = value; }
        }

        /// <summary>
        /// CellHeight
        /// </summary>
        public int CellHeight
        {
            get { return ColorPanel.CellHeight; }
            set { ColorPanel.CellHeight = value; }
        }

        /// <summary>
        /// Columns
        /// </summary>
        public int Columns
        {
            get { return ColorPanel.Columns; }
            set { ColorPanel.Columns = value; }
        }

        /// <summary>
        /// Rows
        /// </summary>
        public int Rows
        {
            get { return ColorPanel.Rows; }
            set { ColorPanel.Rows = value; }
        }

        /// <summary>
        /// CellPadding
        /// </summary>
        public int CellPadding
        {
            get { return ColorPanel.CellPadding; }
            set { ColorPanel.CellPadding = value; }
        }

        /// <summary>
        /// ColorList
        /// </summary>
        public List<Color> ColorList
        {
            get { return ColorPanel.ColorList; }
            set { ColorPanel.ColorList = value; }
        }

        /// <summary>
        /// CurrentColor
        /// </summary>
        public Color CurrentColor
        {
            get { return ColorPanel.CurrentColor; }
            set { ColorPanel.CurrentColor = value; }
        }
        #endregion

        #region methods
        /// <summary>
        /// Constructor
        /// </summary>
        public ToolStripInlineColorPicker()
            : base(new ColorTilePanel())
        { }

        public ToolStripInlineColorPicker(Control c)
            : base(c)
        { }
        #endregion

        #region handlers
        protected override void OnSubscribeControlEvents(Control control)
        {
            base.OnSubscribeControlEvents(control);

            ColorTilePanel panel = control as ColorTilePanel;
            panel.ColorSelected += panel_ColorSelected;
        }

        protected override void OnUnsubscribeControlEvents(Control control)
        {
            base.OnUnsubscribeControlEvents(control);

            ColorTilePanel panel = control as ColorTilePanel;
            panel.ColorSelected -= panel_ColorSelected;
        }

        void panel_ColorSelected(object sender, ColorSelectedEventArgs e)
        {
            OnColorSelected(e);
        }
        #endregion
    }
}
