﻿namespace E_Signature
{
    partial class E_SignatureForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(E_SignatureForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripInlineBrushPicker1 = new CustomToolStrip.ToolStripInlineBrushPicker();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripInlineColorPicker1 = new CustomToolStrip.ToolStripInlineColorPicker();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.清除ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.鎖定ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.送出ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripInlineBrushPicker1,
            this.toolStripMenuItem1,
            this.toolStripInlineColorPicker1,
            this.toolStripMenuItem2,
            this.清除ToolStripMenuItem,
            this.鎖定ToolStripMenuItem,
            this.送出ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 54);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripInlineBrushPicker1
            // 
            this.toolStripInlineBrushPicker1.BrushList = ((System.Collections.Generic.List<int>)(resources.GetObject("toolStripInlineBrushPicker1.BrushList")));
            this.toolStripInlineBrushPicker1.CellHeight = 40;
            this.toolStripInlineBrushPicker1.CellPadding = 3;
            this.toolStripInlineBrushPicker1.CellWidth = 40;
            this.toolStripInlineBrushPicker1.Columns = 2;
            this.toolStripInlineBrushPicker1.CurrentMode = 1;
            this.toolStripInlineBrushPicker1.Name = "toolStripInlineBrushPicker1";
            this.toolStripInlineBrushPicker1.Rows = 1;
            this.toolStripInlineBrushPicker1.Size = new System.Drawing.Size(94, 47);
            this.toolStripInlineBrushPicker1.Text = "toolStripInlineBrushPicker1";
            this.toolStripInlineBrushPicker1.BrushSelected += new CustomToolStrip.BrushSelectedEventHandler(this.toolStripInlineBrushPicker1_BrushSelected);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Font = new System.Drawing.Font("微軟正黑體", 18F);
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(32, 50);
            this.toolStripMenuItem1.Text = "|";
            // 
            // toolStripInlineColorPicker1
            // 
            this.toolStripInlineColorPicker1.CellHeight = 40;
            this.toolStripInlineColorPicker1.CellPadding = 3;
            this.toolStripInlineColorPicker1.CellWidth = 40;
            this.toolStripInlineColorPicker1.ColorList = ((System.Collections.Generic.List<System.Drawing.Color>)(resources.GetObject("toolStripInlineColorPicker1.ColorList")));
            this.toolStripInlineColorPicker1.Columns = 5;
            this.toolStripInlineColorPicker1.CurrentColor = System.Drawing.Color.Transparent;
            this.toolStripInlineColorPicker1.Name = "toolStripInlineColorPicker1";
            this.toolStripInlineColorPicker1.Rows = 1;
            this.toolStripInlineColorPicker1.Size = new System.Drawing.Size(235, 47);
            this.toolStripInlineColorPicker1.Text = "toolStripInlineColorPicker1";
            this.toolStripInlineColorPicker1.ColorSelected += new CustomToolStrip.ColorSelectedEventHandler(this.toolStripInlineColorPicker1_ColorSelected);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Font = new System.Drawing.Font("微軟正黑體", 18F);
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(32, 50);
            this.toolStripMenuItem2.Text = "|";
            // 
            // 清除ToolStripMenuItem
            // 
            this.清除ToolStripMenuItem.AutoSize = false;
            this.清除ToolStripMenuItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.清除ToolStripMenuItem.Font = new System.Drawing.Font("微軟正黑體", 16F);
            this.清除ToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("清除ToolStripMenuItem.Image")));
            this.清除ToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.清除ToolStripMenuItem.Name = "清除ToolStripMenuItem";
            this.清除ToolStripMenuItem.Size = new System.Drawing.Size(50, 50);
            this.清除ToolStripMenuItem.Text = "清除";
            this.清除ToolStripMenuItem.Click += new System.EventHandler(this.清除ToolStripMenuItem_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(10, 58);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(784, 435);
            this.panel1.TabIndex = 4;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(18, 15);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(750, 400);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            this.pictureBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // 鎖定ToolStripMenuItem
            // 
            this.鎖定ToolStripMenuItem.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.鎖定ToolStripMenuItem.Name = "鎖定ToolStripMenuItem";
            this.鎖定ToolStripMenuItem.Size = new System.Drawing.Size(60, 50);
            this.鎖定ToolStripMenuItem.Text = "鎖定";
            this.鎖定ToolStripMenuItem.Click += new System.EventHandler(this.鎖定ToolStripMenuItem_Click);
            // 
            // 送出ToolStripMenuItem
            // 
            this.送出ToolStripMenuItem.Font = new System.Drawing.Font("微軟正黑體", 14F);
            this.送出ToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.送出ToolStripMenuItem.Name = "送出ToolStripMenuItem";
            this.送出ToolStripMenuItem.Size = new System.Drawing.Size(60, 50);
            this.送出ToolStripMenuItem.Text = "送出";
            this.送出ToolStripMenuItem.Click += new System.EventHandler(this.送出ToolStripMenuItem_Click);
            // 
            // E_SignatureForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 540);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "E_SignatureForm";
            this.Text = "E_SignatureForm";
            this.Load += new System.EventHandler(this.E_Signature_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private CustomToolStrip.ToolStripInlineBrushPicker toolStripInlineBrushPicker1;
        private CustomToolStrip.ToolStripInlineColorPicker toolStripInlineColorPicker1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem 清除ToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolStripMenuItem 鎖定ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 送出ToolStripMenuItem;
    }
}