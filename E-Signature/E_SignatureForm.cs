﻿using CustomToolStrip;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace E_Signature
{
    public partial class E_SignatureForm : Form
    {
        private Color penColor = new Color();
        private int transparency = 255;
        private int penWidth = 2;

        private Image CurrentImage;

        // 定義兩個點 (起點、終點)
        private System.Drawing.Point p1, p2;

        // 設置一個啟動標誌
        private static bool drawing = false;

        public E_SignatureForm()
        {
            InitializeComponent();
        }

        private void E_Signature_Load(object sender, EventArgs e)
        {
            penColor = Color.Black;
            toolStripInlineColorPicker1.CurrentColor = penColor;

            鎖定ToolStripMenuItem.Enabled = false;

            //pictureBox1.Image = Image.FromFile(@"456.jpg");

            CurrentImage = (Image) pictureBox1.Image.Clone();
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            pictureBox1.Cursor = Cursors.Cross;

            if (e.Button == MouseButtons.Right)
                return;

            if (drawing)
            {
                鎖定ToolStripMenuItem.Enabled = true;

                System.Drawing.Point currentPoint = new System.Drawing.Point(e.X, e.Y);

                Graphics temp_g = pictureBox1.CreateGraphics();
                temp_g.SmoothingMode = SmoothingMode.AntiAlias;
                temp_g.DrawLine(new Pen(Color.FromArgb(transparency, penColor), penWidth), p2, currentPoint);

                Graphics g = Graphics.FromImage(CurrentImage);
                g.SmoothingMode = SmoothingMode.AntiAlias;
                g.DrawLine(new Pen(Color.FromArgb(transparency, penColor), penWidth), p2, currentPoint);

                p2.X = currentPoint.X;
                p2.Y = currentPoint.Y;
            }
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            p1 = new System.Drawing.Point(e.X, e.Y);
            p2 = new System.Drawing.Point(e.X, e.Y);
            drawing = true;
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            drawing = false;
        }

        private void toolStripInlineBrushPicker1_BrushSelected(object sender, BrushSelectedEventArgs e)
        {
            if (e.SelectedMode == 1)
            {
                penWidth = 2;
                transparency = 255;
            }

            else if (e.SelectedMode == 2)
            {
                transparency = 60;
                penWidth = 20;
            }
        }

        private void 清除ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            鎖定ToolStripMenuItem.Enabled = false;
            
            CurrentImage = (Image)pictureBox1.Image.Clone();
            pictureBox1.Refresh();
        }

        private void 鎖定ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            鎖定ToolStripMenuItem.Enabled = false;
            pictureBox1.Image = (Image) CurrentImage.Clone();
        }

        private void 送出ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(鎖定ToolStripMenuItem.Enabled)
            {
                MessageBox.Show("內容尚未鎖定, 無法送出!");
                return;
            }
        }

        private void toolStripInlineColorPicker1_ColorSelected(object sender, CustomToolStrip.ColorSelectedEventArgs e)
        {
            penColor = e.SelectedColor;

            var item = menuStrip1.Items.Find("toolStripInlineColorPicker1", false);
            (item[0] as ToolStripInlineColorPicker).CurrentColor = penColor;
        }

    }
}
